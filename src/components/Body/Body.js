import React, {useEffect, useState} from 'react';
import {Switch, Route} from "react-router-dom";

import MainPage from "../Pages/MainPage/MainPage";
import Cart from "../Pages/Cart/Cart";
import Favourite from "../Pages/Favourites/Favourite";

import "./Body.scss";

const Body = () => {

    const [goods, setGoods] = useState([]);
    const [goodsInCartID, setGoodsInCartID] = useState([]);
    const [goodsInFavouriteID, setGoodsInFavouriteID] = useState([]);
    const [showModalWindow, setShowModalWindow] = useState(false);
    const [addedItemId, setAddedItemID] = useState(null);


    useEffect(() => {
        getGoodsFromJson();
        addKeyToLocalStorage("Cart");
        addKeyToLocalStorage("Favourite");
        getCartFromLocalstorage();
        getFavouriteFromLocalstorage();
    }, []);


    useEffect(() => {
        localStorage.setItem("Cart", JSON.stringify(goodsInCartID));
        localStorage.setItem("Favourite", JSON.stringify(goodsInFavouriteID));
    }, [goodsInCartID, goodsInFavouriteID])

    const getGoodsFromJson = async () => {
        const goodsSource = "./baza.json"
        const receivedGoodsData = await fetch(goodsSource);
        const goodsData = await receivedGoodsData.json();
        setGoods(goodsData)
    }

    const addKeyToLocalStorage = (key) => {
        if (!localStorage.getItem(key)) {
            (localStorage.setItem(key, JSON.stringify([])))
        }
    }

    const getCartFromLocalstorage = () => {
        const data = JSON.parse(localStorage.getItem("Cart"));
        setGoodsInCartID(data)

    }

    const getFavouriteFromLocalstorage = () => {
        const data = JSON.parse(localStorage.getItem("Favourite"));
        setGoodsInFavouriteID(data)
    }


    const showModal = () => {
        setShowModalWindow(true)

        document.body.classList.add("modal-open");
    }

    const closeModal = () => {
        setShowModalWindow(false)

        document.body.classList.remove("modal-open");
    }


    const setFavourite = (id) => {
        const isIdExist = goodsInFavouriteID.includes(id);
        if (!isIdExist) {
            setGoodsInFavouriteID([...goodsInFavouriteID, id])

        } else {
            const newData = goodsInFavouriteID.filter(e => e !== id);
            setGoodsInFavouriteID(newData)
        }
    }

    const updateDataInCart = (value) => {
        setGoodsInCartID(value);
        localStorage.setItem("Cart", JSON.stringify(goodsInCartID));
    }

    const addToCart = (id) => {
        const isIdExist = goodsInCartID.includes(id);

        if (!isIdExist) {
            updateDataInCart([...goodsInCartID, id]);
            setAddedItemID(null);
        }
    }

    const handleAddClick = (id) => {
        showModal();
        setAddedItemID(id);
    }

    const handleOKClick = () => {
        addToCart(addedItemId);
        closeModal();
    }

    // const updateDataInFavourite = (value) => {
    //     setGoodsInFavouriteID(value);
    //     localStorage.setItem("Favourite", JSON.stringify(goodsInFavouriteID));
    // }

    return (
        <div className="page">
            <Switch>
                <Route path="/" exact>
                    <MainPage goodsData={goods}
                              goodsInCartID={goodsInCartID}
                              goodsInFavouriteID={goodsInFavouriteID}
                              setFavourite={setFavourite}
                              closeModal={closeModal}
                              showModal={showModal}
                              showModalWindow={showModalWindow}
                              updateDataInCart={updateDataInCart}
                              handleAddClick={handleAddClick}
                              handleOKClick={handleOKClick}
                              addedItemId={addedItemId}
                    />
                </Route>
                <Route path="/cart" exact>
                    <Cart goods={goods}
                          goodsInCartID={goodsInCartID}
                          closeModal={closeModal}
                          showModalWindow={showModalWindow}
                          showModal={showModal}
                          updateDataInCart={updateDataInCart}/>
                </Route>
                <Route path="/favourite" exact>
                    <Favourite goods={goods}
                               goodsInCartID={goodsInCartID}
                               goodsInFavouriteID={goodsInFavouriteID}
                               setFavourite={setFavourite}
                               closeModal={closeModal}
                               showModal={showModal}
                               showModalWindow={showModalWindow}
                               updateDataInCart={updateDataInCart}
                               handleAddClick={handleAddClick}
                               handleOKClick={handleOKClick}
                               addedItemId={addedItemId}/>

                </Route>
            </Switch>
        </div>
    );
}


export default Body;