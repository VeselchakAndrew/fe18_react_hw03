import React, {useEffect, useState} from "react";
import Card from "../../Card/Card";
import CustomButton from "../../CustomButton/CustomButton";
import Button from "../../Button/Button";
import ModalWindow from "../../ModalWindow/ModalWindow";

const Cart = (props) => {

    const {goods, closeModal, showModalWindow, showModal, goodsInCartID, updateDataInCart} = props;

    const [goodsInCart, setGoodsInCart] = useState([]);
    const [pageIsNotEmpty, setPageIsNotEmpty] = useState(true)
    const [itemIdToRemove, setItemIdToRemove] = useState(null);


    useEffect(() => {
        const intersect = goods.filter(e => goodsInCartID.some((item) => e.vendorCode === item))
        setGoodsInCart(intersect);
    }, [goods, goodsInCartID]);

    useEffect(() => {
        if (goodsInCart.length > 0) {
            setPageIsNotEmpty(false)
        } else {
            setPageIsNotEmpty(true)
        }
    }, [goodsInCart])

    const removeBtnHandler = (id) => {
        setItemIdToRemove(id);
        showModal();
    }

    const removeFromCart = (id) => {
        const newData = goodsInCartID.filter(e => id !== e);
        updateDataInCart(newData);

        setItemIdToRemove(null);
        closeModal();
    }


    const cards = goodsInCart.map(good => (
            <div key={good.vendorCode}>
                <Card good={good} children={
                    <>
                        <CustomButton customBtnClass='delete_btn'
                                      btnAction={() => removeBtnHandler(good.vendorCode)}/>
                    </>}
                />
            </div>
        )
    )

    return (
        <>
            {pageIsNotEmpty &&
            <div className="empty_page">
                <h2>Корзина пустая</h2>
            </div>}
            {cards}
            {showModalWindow && <ModalWindow closeWindow={closeModal}
                                             info={"Вы действительно хотите удалить товар из корзины?"}
                                             children={
                                                 <>
                                                     <Button
                                                         btnAction={() => removeFromCart(itemIdToRemove)}>OK</Button>
                                                     <Button btnAction={closeModal}>Cancel</Button>
                                                 </>
                                             }
            />}
        </>
    )
}

export default Cart;

