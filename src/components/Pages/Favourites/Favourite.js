import React, {useEffect, useState} from "react";
import Card from "../../Card/Card";
import CustomButton from "../../CustomButton/CustomButton";
import Button from "../../Button/Button";
import ModalWindow from "../../ModalWindow/ModalWindow";

const Favourite = (props) => {
    const {goods, setFavourite, goodsInFavouriteID, showModalWindow, closeModal, goodsInCartID, handleAddClick, handleOKClick, addedItemId} = props;

    const [goodsInFavourite, setGoodsInFavourite] = useState([]);
    const [pageIsNotEmpty, setPageIsNotEmpty] = useState(true)

    useEffect(() => {
        const intersect = goods.filter(e => goodsInFavouriteID.some((item) => e.vendorCode === item))
        setGoodsInFavourite(intersect);
    }, [goods, goodsInFavouriteID]);

    useEffect(() => {
        if (goodsInFavouriteID.length > 0) {
            setPageIsNotEmpty(false)
        }
        else {
            setPageIsNotEmpty(true)
        }
    }, [goodsInFavouriteID])

    const cards = goodsInFavourite.map(good => (
            <div key={good.vendorCode}>
                <Card good={good} children={
                    <>
                        <Button btnClass="add_btn" btnAction={() => handleAddClick(good.vendorCode)}>
                            {goodsInCartID.includes(good.vendorCode) ? "Already in cart" : "Add to cart"}
                        </Button>
                        <CustomButton
                            customBtnClass={goodsInFavouriteID.includes(good.vendorCode)
                                ? "favourite  favourite_is_checked"
                                : "favourite"}
                            btnAction={() => setFavourite(good.vendorCode)}
                        />
                    </>}
                />
            </div>
        )
    )

    return (
        <>
            {pageIsNotEmpty &&
            <div className="empty_page">
                <h2>В избранном ничего нет :(</h2>
            </div>}
            {cards}
            {showModalWindow &&
            !goodsInCartID.includes(addedItemId) &&
            <ModalWindow closeWindow={closeModal}
                         info={"Добавить товар в корзину?"}
                         children={
                             <>
                                 <Button btnClass="add_btn" btnAction={handleOKClick}>OK</Button>
                                 <Button btnClass="add_btn"
                                         btnAction={closeModal}>Cancel</Button>
                             </>}


            />}

            {showModalWindow &&
            goodsInCartID.includes(addedItemId) &&
            <ModalWindow closeWindow={closeModal}
                         info={"Товар уже в корзине!"}
                         children={
                             <Button btnClass="add_btn" btnAction={closeModal}>Cancel</Button>
                         }
            />}
        </>
    )
}

export default Favourite;