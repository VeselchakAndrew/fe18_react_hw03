import React from 'react';
import Card from "../../Card/Card";
import Button from "../../Button/Button";
import CustomButton from "../../CustomButton/CustomButton";
import "./MainPage.scss"
import PropTypes from "prop-types";
import ModalWindow from "../../ModalWindow/ModalWindow";


const MainPage = (props) => {

    const {goodsData, goodsInCartID, goodsInFavouriteID, setFavourite, closeModal, handleAddClick, handleOKClick, showModalWindow, addedItemId} = props;


    const cards = goodsData.map(good => (
            <div key={good.vendorCode}>
                <Card good={good} children={
                    <>
                        <Button btnClass="add_btn" btnAction={() => handleAddClick(good.vendorCode)}>
                            {goodsInCartID.includes(good.vendorCode) ? "Already in cart" : "Add to cart"}
                        </Button>
                        <CustomButton
                            customBtnClass={goodsInFavouriteID.includes(good.vendorCode)
                                ? "favourite  favourite_is_checked"
                                : "favourite"}
                            btnAction={() => setFavourite(good.vendorCode)}
                        />
                    </>}
                />
            </div>
        )
    )

    return (
        <>
            {cards}
            {showModalWindow &&
            !goodsInCartID.includes(addedItemId) &&
            <ModalWindow closeWindow={closeModal}
                         info={"Добавить товар в корзину?"}
                         children={
                             <>
                                 <Button btnClass="add_btn" btnAction={handleOKClick}>OK</Button>
                                 <Button btnClass="add_btn"
                                         btnAction={closeModal}>Cancel</Button>
                             </>}


            />}

            {showModalWindow &&
            goodsInCartID.includes(addedItemId) &&
            <ModalWindow closeWindow={closeModal}
                         info={"Товар уже в корзине!"}
                         children={
                             <Button btnClass="add_btn" btnAction={closeModal}>Cancel</Button>
                         }
            />}
        </>
    );


}

MainPage.propTypes = {
    goodsData: PropTypes.array.isRequired,
    goodsInCartID: PropTypes.array,
    goodsInFavouriteID: PropTypes.array,
    updateDataInCart: PropTypes.func.isRequired,
    closeModal: PropTypes.func.isRequired,
    showModal: PropTypes.func.isRequired,
    showModalWindow: PropTypes.bool.isRequired,
    setFavourite: PropTypes.func.isRequired,
}

MainPage.defaultProps = {
    goodsInCartID: [],
    goodsInFavouriteID: [],
}

export default MainPage;