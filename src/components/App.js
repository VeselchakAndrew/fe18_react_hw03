import './App.scss';
import React from "react";
import Header from "./Header/Header"
import Footer from "./Footer/Footer";
import Body from "./Body/Body";
import {BrowserRouter as Router} from "react-router-dom"

const App = () => {

    return (
        <div className="app">
            <Router>
                <Header/>
                <Body/>
            </Router>
            <Footer/>
        </div>
    );
}

export default App;
